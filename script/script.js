"use strict";

let inputNumber = +prompt("Please, enter your number:");

function validation () {
    while (isNaN(inputNumber) || inputNumber === "" || !Number.isInteger(inputNumber)) {
        alert("You have not entered a number or entered incorrect one!!")

        inputNumber = +prompt("Please, enter correct first number:");
    }
}
validation();

function fibonacci(number) {
    if (number >= 0) {
        let previous = 0, next = 1;
        for(let i = 0; i < number; i++){
            let temporaryVariable = next;
            next = previous + next;
            previous = temporaryVariable;
        }
        return previous;
    } else {
        let previous = 0, next = -1;
        for(let i = 0; i > number; i--){
            let temporaryVariable = next;
            next = previous + next;
            previous = temporaryVariable;
        }
        return previous;
    }

}

console.log(fibonacci(inputNumber));